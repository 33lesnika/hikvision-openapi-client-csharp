using Microsoft.VisualStudio.TestTools.UnitTesting;
using Hikvision_Artemis_Api;
using System;
using System.Text.Json;
using Hikvision_Artemis_Api.http;

namespace Hikvision_Artemis_Api_tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void DeserializerTest()
        {
            var underTest = new ArtemisHttpApi("http://example.com", "NO_KEY", "NO_SECRET").options;
            EventMessage deserialized = JsonSerializer.Deserialize<EventMessage>(getTestEvent(), underTest);
            Assert.AreEqual("OnEventNotify", deserialized.Method);
            Event value = deserialized.Params.Events[0];
            Assert.IsTrue((value.GetType() == typeof(AccessControlEvent)));
            Console.WriteLine(JsonSerializer.Serialize(value as AccessControlEvent));
            Assert.AreEqual((value as AccessControlEvent).Data.PersonId, "1");
        }

        public String getTestEvent()
        {
            return System.IO.File.ReadAllText(@"event.json");
        }

        [TestMethod]
        public void SignerTest()
        {
            ArtemisRequestSigner underTest = new ArtemisRequestSigner("NO_KEY", "NO_SECRET");
            string signData = underTest.ComposeStringToSign(new SignData()
            {
                Method = "POST",
                Accept = "application/json",
                ContentType = "application/json;charset=UTF-8",
                Nonce = "2a0baf8f-e73c-410a-b402-8916fe255089",
                Timestamp = "123",
                Path = "/api",
            });

            string expectedSignData = "POST\napplication/json\napplication/json;charset=UTF-8\nx-ca-key:NO_KEY\nx-ca-nonce:2a0baf8f-e73c-410a-b402-8916fe255089\nx-ca-timestamp:123\n/api";

            Console.WriteLine(signData);
            Assert.AreEqual(expectedSignData, signData);
            string expectedSignature = "SLvUMz0wSnBCEUEXrJA6pPYoYdMaWPf4XqEctDXqtAg=";
            string actualSignature = underTest.ComputeSignature(signData);
            Assert.AreEqual(expectedSignature, actualSignature);
        }

        [TestMethod]
        [ExpectedException(typeof(ArtemisException))]
        public void FaitAddPersonTest()
        {
            var underTest = new ArtemisHttpApi("http://example.com/artemis/", "NO_KEY", "NO_SECRET");
            var result = underTest.AddPerson(new AddPersonRequest());
            Console.WriteLine(result.Code);
        }

        [TestMethod]
        public void AddPersonTest()
        {
            var underTest = new ArtemisHttpApi("https://192.168.10.170/artemis/", "25030668", "w6Z3UuxBUhBjr64W7CV8", debug: true);
            var result = underTest.AddPerson(new AddPersonRequest()
            {
                OrgIndexCode = "1",
                PersonFamilyName = "Golubnichiy",
                PersonGivenName = "Vladimir"
            });
            Console.WriteLine(result.Data);
        }

        [TestMethod]
        public void GetOrganizationsListTest()
        {
            var underTest = new ArtemisHttpApi("https://192.168.10.170/artemis/", "25030668", "w6Z3UuxBUhBjr64W7CV8", debug: true);
            var result = underTest.GetOrganizationsList();
            Assert.IsTrue(result.Count > 0);
            Console.WriteLine(result[0].OrgIndexCode);
        }
    }
}
