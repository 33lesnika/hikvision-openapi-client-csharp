﻿using Hikvision_Artemis_Api.message;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hikvision_Artemis_Api
{

    public interface IArtemisApi
    {
        VersionResponse GetVersion();
        Task<VersionResponse> GetVersionAsync();
        AddOrganizationResponse AddOrganization(AddOrganizationRequest request);
        Task<AddOrganizationResponse> AddOrganizationAsync(AddOrganizationRequest request);
        OrganizationsResponse GetOrganizations(OrganizationsRequest request);
        Task<OrganizationsResponse> GetOrganizationsAsync(OrganizationsRequest request);
        List<OrganizationInfo> GetOrganizationsList();
        AddPersonResponse AddPerson(AddPersonRequest request);
        Task<AddPersonResponse> AddPersonAsync(AddPersonRequest request);
        UpdatePersonResponse UpdatePerson(UpdatePersonRequest request);
        Task<UpdatePersonResponse> UpdatePersonAsync(UpdatePersonRequest request);
        DeletePersonResponse DeletePerson(DeletePersonRequest request);
        Task<DeletePersonResponse> DeletePersonAsync(DeletePersonRequest request);
        UpdatePersonFaceResponse UpdatePersonFace(UpdatePersonFaceRequest request);
        Task<UpdatePersonFaceResponse> UpdatePersonFaceAsync(UpdatePersonFaceRequest request);
        PersonInfoResponse GetPersonInfoByPersonCode(PersonInfoByCodeRequest request);
        Task<PersonInfoResponse> GetPersonInfoByPersonCodeAsync(PersonInfoByCodeRequest request);
        PersonInfoResponse GetPersonInfoByPersonId(PersonInfoByPersonIdRequest request);
        Task<PersonInfoResponse> GetPersonInfoByPersonIdAsync(PersonInfoByPersonIdRequest request);
        SubscribeByEventTypesResponse SubscribeByEventTypes(SubscribeByEventTypesRequest request);
        Task<SubscribeByEventTypesResponse> SubscribeByEventTypesAsync(SubscribeByEventTypesRequest request);
        CancelSubscribeByEventTypesResponse CancelSubscriptionByEventTypes(CancelSubscribeByEventTypesRequest request);
        Task<CancelSubscribeByEventTypesResponse> CancelSubscriptionByEventTypesAsync(CancelSubscribeByEventTypesRequest request);
        EventImageDataReponse GetEventImageData(EventImageDataRequest request);
        Task<EventImageDataReponse> GetEventImageDataAsync(EventImageDataRequest request);
        public void StartServer(int tcpPort);
        event EventHandler<EventMessage> OnEventNotify;
    }
}
