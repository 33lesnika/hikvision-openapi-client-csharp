﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Hikvision_Artemis_Api
{
    public class PersonInfo
    {
        [MaxLength(16)]
        public string PersonCode { get; set; }
        [Required]
        [MaxLength(256)]
        public string PersonFamilyName { get; set; }
        [Required]
        [MaxLength(256)]
        public string PersonGivenName { get; set; }
        // 0 - unknown; 1 - male; 2 - female
        [Range(0, 2)]
        public int Gender { get; set; }
        [Required]
        public string OrgIndexCode { get; set; }
        public string PhoneNo { get; set; }
        [EmailAddress]
        public string Email { get; set; }
        public List<string> Faces { get; set; }
        public List<FingerPrint> FingerPrint { get; set; }
        [MaxLength(128)]
        public string Remark { get; set; }
        public List<Card> Cards { get; set; }
        public string BeginTime { get; set; }
        public string EndTime { get; set; }
        public List<CustomField> CustomFields { get; set; }
    }
}
