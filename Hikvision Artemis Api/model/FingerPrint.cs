﻿using System.ComponentModel.DataAnnotations;

namespace Hikvision_Artemis_Api
{
    public class FingerPrint
    {
        [MaxLength(64)]
        public string FingerPrintIndexCode { get; set; }
        [MaxLength(64)]
        public string FingerPrintName { get; set; }
        [MaxLength(1024)]
        public string FingerPrintData { get; set; }
        [MaxLength(20)]
        public string RelatedCardNo { get; set; }
    }
}
