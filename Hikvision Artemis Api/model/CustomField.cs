﻿namespace Hikvision_Artemis_Api
{
    public class CustomField
    {
        public string Id { get; set; }
        public string CustomFieldName { get; set; }
        public int CustomFieldType { get; set; }
        public string CustomFieldValue { get; set; }
    }
}
