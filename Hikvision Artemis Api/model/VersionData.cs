﻿namespace Hikvision_Artemis_Api
{
    public class VersionData
    {
        public string ProductName { get; set; }
        public string SoftVersion { get; set; }
    }
}