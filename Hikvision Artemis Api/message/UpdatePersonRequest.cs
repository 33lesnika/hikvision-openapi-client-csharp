﻿using System.Text.Json.Serialization;

namespace Hikvision_Artemis_Api
{
    public class UpdatePersonRequest : PersonInfo
    {
        [JsonIgnore]
        [HeaderParam]
        public string UserId { get; set; }
    }
}