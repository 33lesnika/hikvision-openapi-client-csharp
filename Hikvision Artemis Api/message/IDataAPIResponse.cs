﻿namespace Hikvision_Artemis_Api
{
    public class IDataAPIResponse<T> : IAPIResponse
    {
        public virtual T Data { get; set; }
    }
}
