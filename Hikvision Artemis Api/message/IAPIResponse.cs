﻿namespace Hikvision_Artemis_Api
{
    public class IAPIResponse
    {
        public string Code { get; set; }
        public string Msg { get; set; }
    }
}
