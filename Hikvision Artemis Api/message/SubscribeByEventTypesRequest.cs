﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Hikvision_Artemis_Api
{
    public class SubscribeByEventTypesRequest
    {
        [Required]
        [MaxLength(256)]
        public List<int> EventTypes { get; set; }
        [Required]
        [MaxLength(1024)]
        // HTTP URL of RESTful callback service
        public string EventDest { get; set; }
        public string Token { get; set; }
    }
}
