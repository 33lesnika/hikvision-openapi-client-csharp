﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Hikvision_Artemis_Api.message
{
    public class PersonInfoByPersonIdRequest
    {
        [JsonIgnore]
        [HeaderParam]
        public string UserId { get; set; }
        [Required]
        [MaxLength(64)]
        public string PersonId { get; set; }
    }
}
