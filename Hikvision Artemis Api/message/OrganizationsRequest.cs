﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Hikvision_Artemis_Api
{
    public class OrganizationsRequest
    {
        [JsonIgnore]
        [HeaderParam]
        public string UserId { get; set; }
        [Required]
        [Range(0, int.MaxValue)]
        public int PageNo { get; set; }
        [Required]
        [Range(1, 500)]
        public int PageSize { get; set; }
    }
}