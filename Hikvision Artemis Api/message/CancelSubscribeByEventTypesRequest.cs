﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Hikvision_Artemis_Api
{
    public class CancelSubscribeByEventTypesRequest
    {
        [Required]
        [MaxLength(256)]
        public List<int> EventTypes { get; set; }
    }
}
