﻿using System.Text.Json.Serialization;

namespace Hikvision_Artemis_Api
{
    public class AddPersonRequest : PersonInfo
    {
        [JsonIgnore]
        [HeaderParam]
        public string UserId { get; set; }
    }
}
