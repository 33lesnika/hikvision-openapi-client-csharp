﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Hikvision_Artemis_Api
{
    public class Page<T>
    {
        [Range(0, int.MaxValue)]
        public int PageNo { get; set; }
        [Range(1, 500)]
        public int PageSize { get; set; }
        public int Total { get; set; }
        public List<T> List { get; set; }
    }
}