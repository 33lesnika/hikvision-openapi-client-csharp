﻿using System.ComponentModel.DataAnnotations;

namespace Hikvision_Artemis_Api
{
    public class OrganizationInfo
    {
        [MaxLength(64)]
        public string OrgIndexCode { get; set; }
        [MaxLength(64)]
        public string OrgName { get; set; }
        [MaxLength(64)]
        public string ParentOrgIndexCode { get; set; }
    }
}