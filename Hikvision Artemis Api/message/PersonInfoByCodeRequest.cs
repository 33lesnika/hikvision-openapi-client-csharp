﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Hikvision_Artemis_Api
{
    public class PersonInfoByCodeRequest
    {
        [JsonIgnore]
        [HeaderParam]
        public string UserId { get; set; }
        [Required]
        [MaxLength(64)]
        public string PersonCode { get; set; }
    }

}