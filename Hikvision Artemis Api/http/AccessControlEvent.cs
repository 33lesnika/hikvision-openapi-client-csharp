﻿
using Dahomey.Json.Attributes;

namespace Hikvision_Artemis_Api.http
{
    [JsonDiscriminator(196893)]
    public class AccessControlEvent : Event
    {
        public AccessControlEventData Data { get; set; }
    }
}
