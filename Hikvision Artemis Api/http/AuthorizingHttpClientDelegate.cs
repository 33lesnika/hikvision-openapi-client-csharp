﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Hikvision_Artemis_Api
{
    internal class AuthorizingHttpClientDelegate : HttpClient
    {
        private readonly HttpClient _httpClient;
        private readonly ArtemisRequestSigner _artemisRequestSigner;

        public AuthorizingHttpClientDelegate(ArtemisRequestSigner signer, HttpClient httpClient)
        {
            _httpClient = httpClient;
            _artemisRequestSigner = signer;
        }

        public override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            _artemisRequestSigner.Sign(request);
            return _httpClient.SendAsync(request, cancellationToken);
        }

        public override bool Equals(object obj)
        {
            return _httpClient.Equals(obj);
        }

        public override int GetHashCode()
        {
            return _httpClient.GetHashCode();
        }

        public override string ToString()
        {
            return _httpClient.ToString();
        }

        protected override void Dispose(bool disposing)
        {
            _httpClient.Dispose();
        }
    }
}