﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Hikvision_Artemis_Api
{
    public class EventParam
    {
        [Required]
        public string SendTime { get; set; }
        [Required]
        public string Ability { get; set; }
        [Required]
        public List<Event> Events { get; set; }
    }
}