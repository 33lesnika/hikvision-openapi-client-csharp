﻿using System.ComponentModel.DataAnnotations;

namespace Hikvision_Artemis_Api.http
{
    public class AccessControlEventData
    {
        [Required]
        public string PersonId { get; set; }
        public string PersonCode { get; set; }
        [Required]
        public string CardNo { get; set; }
        [Required]
        public int CheckInAndOutType { get; set; }
        public string PicUri { get; set; }
        public string TemperatureData { get; set; }
        public int TemperatureStatus { get; set; }

        public int WearMaskStatus { get; set; }
        public string ReaderIndexCode { get; set; }
        public string ReaderName { get; set; }
    }
}
