﻿
namespace Hikvision_Artemis_Api.http
{
    public class StringEvent : Event
    {
        public string Data { get; set; }
    }
}
