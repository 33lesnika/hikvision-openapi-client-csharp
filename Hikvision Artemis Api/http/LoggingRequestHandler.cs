﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Hikvision_Artemis_Api
{
    internal class LoggingRequestHandler : DelegatingHandler
    {
        private readonly bool _debug;
        public LoggingRequestHandler(bool debug, DelegatingHandler next): base(next)
        {
            _debug = debug;
        }
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            if (!_debug)
            {
                return await base.SendAsync(request, cancellationToken);
            }
            Console.WriteLine($"{DateTime.Now} Request:");
            Console.WriteLine(request.ToString());
            if (request.Content != null)
            {
                Console.WriteLine(await request.Content.ReadAsStringAsync());
            }
            Console.WriteLine();

            HttpResponseMessage response = await base.SendAsync(request, cancellationToken);

            Console.WriteLine($"{DateTime.Now} Response:");
            Console.WriteLine(response.ToString());
            if (response.Content != null)
            {
                Console.WriteLine(await response.Content.ReadAsStringAsync());
            }

            return response;
        }
    }
}