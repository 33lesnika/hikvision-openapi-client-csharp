﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Hikvision_Artemis_Api
{
    public class AuthRequestHandler : DelegatingHandler
    {
        private readonly ArtemisRequestSigner _signer;

        public AuthRequestHandler(ArtemisRequestSigner signer, HttpClientHandler next) : base(next)
        {
            _signer = signer;
        }

        public AuthRequestHandler(ArtemisRequestSigner signer)
        {
            _signer = signer;
            InnerHandler = new HttpClientHandler();
        }
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            _signer.Sign(request);
            return base.SendAsync(request, cancellationToken);
        }
    }
}
