﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Hikvision_Artemis_Api
{
    public class EventMessage : EventArgs
    {
        [Required]
        public string Method { get; set; }
        [Required]
        public EventParam Params { get; set; }
    }
}