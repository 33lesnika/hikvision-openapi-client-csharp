﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Hikvision_Artemis_Api.http
{
    public class ArtemisException : Exception
    {
        public ArtemisException()
        {
        }

        public ArtemisException(string message) : base(message)
        {
        }

        public ArtemisException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
