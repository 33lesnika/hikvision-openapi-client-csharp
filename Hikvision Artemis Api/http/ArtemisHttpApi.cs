﻿using Dahomey.Json;
using Dahomey.Json.Serialization.Conventions;
using EmbedIO;
using EmbedIO.Actions;
using EmbedIO.WebApi;
using Hikvision_Artemis_Api.http;
using Hikvision_Artemis_Api.message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading;
using System.Threading.Tasks;

namespace Hikvision_Artemis_Api
{
    public class ArtemisHttpApi : IArtemisApi
    {
        private readonly HttpClient _httpClient;
        public string BaseUrl { get; set; }
        public JsonSerializerOptions options { get; private set; }

        public ArtemisHttpApi(string baseUri, string apiKey, string apiSecret, bool debug = false)
        {
            _httpClient = new HttpClient(new LoggingRequestHandler(debug, new AuthRequestHandler(new ArtemisRequestSigner(apiKey, apiSecret), new HttpClientHandler()
            {
                ServerCertificateCustomValidationCallback = HttpClientHandler.DangerousAcceptAnyServerCertificateValidator
            })));
            _httpClient.BaseAddress = new Uri(ensureBaseUriEndsWithSlash(baseUri));
            createJsonSerializerOptions();
        }

        public ArtemisHttpApi(string baseUri, string apiKey, string apiSecret, HttpClient httpClient)
        {
            _httpClient = new AuthorizingHttpClientDelegate(new ArtemisRequestSigner(apiKey, apiSecret), httpClient);
            _httpClient.BaseAddress = new Uri(ensureBaseUriEndsWithSlash(baseUri));
            createJsonSerializerOptions();
        }

        private string ensureBaseUriEndsWithSlash(string baseUri)
        {
            return !baseUri.EndsWith('/') ? baseUri + '/' : baseUri;
        }

        private void createJsonSerializerOptions()
        {
            options = new JsonSerializerOptions();
            //options.PropertyNameCaseInsensitive = true;
            options.SetupExtensions();
            options.DictionaryKeyPolicy = JsonNamingPolicy.CamelCase;
            options.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
            options.DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull;
            DiscriminatorConventionRegistry registry = options.GetDiscriminatorConventionRegistry();
            registry.ClearConventions();
            registry.RegisterConvention(new DefaultDiscriminatorConvention<int>(options, "eventType"));
            registry.RegisterType<AccessControlEvent>();
        }
        #region "Api methods"
        public VersionResponse GetVersion()
        {
            return processAndSend<string, VersionResponse>("", "api/common/v1/version");
        }

        public async Task<VersionResponse> GetVersionAsync()
        {
            return await processAndSendAsync<string, VersionResponse>("", "api/common/v1/version");
        }

        public AddOrganizationResponse AddOrganization(AddOrganizationRequest request)
        {
            return processAndSend<AddOrganizationRequest, AddOrganizationResponse>(request, "api/resource/v1/org/single/add");
        }

        public async Task<AddOrganizationResponse> AddOrganizationAsync(AddOrganizationRequest request)
        {
            return await processAndSendAsync<AddOrganizationRequest, AddOrganizationResponse>(request, "api/resource/v1/org/single/add"); ;
        }

        public OrganizationsResponse GetOrganizations(OrganizationsRequest request)
        {
            return processAndSend<OrganizationsRequest, OrganizationsResponse>(request, "api/resource/v1/org/orgList");
        }

        public List<OrganizationInfo> GetOrganizationsList()
        {
            List<OrganizationInfo> result = new List<OrganizationInfo>();
            for (int i = 1; i < int.MaxValue; i++)
            {
                var response = GetOrganizations(new OrganizationsRequest()
                {
                    PageNo = i,
                    PageSize = 500
                });
                if ((response.Data?.List?.Count ?? 0) == 0)
                {
                    break;
                }
                result.AddRange(response.Data.List);
            }
            return result;
        }

        public async Task<OrganizationsResponse> GetOrganizationsAsync(OrganizationsRequest request)
        {
            return await processAndSendAsync<OrganizationsRequest, OrganizationsResponse>(request, "api/resource/v1/org/orgList"); ;
        }

        public UpdatePersonFaceResponse UpdatePersonFace(UpdatePersonFaceRequest request)
        {
            return processAndSend<UpdatePersonFaceRequest, UpdatePersonFaceResponse>(request, "api/resource/v1/person/face/update");
        }

        public async Task<UpdatePersonFaceResponse> UpdatePersonFaceAsync(UpdatePersonFaceRequest request)
        {
            return await processAndSendAsync<UpdatePersonFaceRequest, UpdatePersonFaceResponse>(request, "api/resource/v1/person/face/update");
        }

        public AddPersonResponse AddPerson(AddPersonRequest request)
        {
            return processAndSend<AddPersonRequest, AddPersonResponse>(request, "api/resource/v1/person/single/add");
        }

        public async Task<AddPersonResponse> AddPersonAsync(AddPersonRequest request)
        {
            return await processAndSendAsync<AddPersonRequest, AddPersonResponse>(request, "api/resource/v1/person/single/add");
        }

        public UpdatePersonResponse UpdatePerson(UpdatePersonRequest request)
        {
            return processAndSend<UpdatePersonRequest, UpdatePersonResponse>(request, "api/resource/v1/person/single/update");
        }

        public async Task<UpdatePersonResponse> UpdatePersonAsync(UpdatePersonRequest request)
        {
            return await processAndSendAsync<UpdatePersonRequest, UpdatePersonResponse>(request, "api/resource/v1/person/single/update");
        }

        public DeletePersonResponse DeletePerson(DeletePersonRequest request)
        {
            return processAndSend<DeletePersonRequest, DeletePersonResponse>(request, "api/resource/v1/person/single/delete");
        }

        public async Task<DeletePersonResponse> DeletePersonAsync(DeletePersonRequest request)
        {
            return await processAndSendAsync<DeletePersonRequest, DeletePersonResponse>(request, "api/resource/v1/person/single/delete");
        }

        public PersonInfoResponse GetPersonInfoByPersonCode(PersonInfoByCodeRequest request)
        {
            return processAndSend<PersonInfoByCodeRequest, PersonInfoResponse>(request, "api/resource/v1/person/personCode/personInfo");
        }

        public async Task<PersonInfoResponse> GetPersonInfoByPersonCodeAsync(PersonInfoByCodeRequest request)
        {
            return await processAndSendAsync<PersonInfoByCodeRequest, PersonInfoResponse>(request, "api/resource/v1/person/personCode/personInfo");
        }

        public PersonInfoResponse GetPersonInfoByPersonId(PersonInfoByPersonIdRequest request)
        {
            return processAndSend<PersonInfoByPersonIdRequest, PersonInfoResponse>(request, "api/resource/v1/person/personId/personInfo");
        }

        public async Task<PersonInfoResponse> GetPersonInfoByPersonIdAsync(PersonInfoByPersonIdRequest request)
        {
            return await processAndSendAsync<PersonInfoByPersonIdRequest, PersonInfoResponse>(request, "api/resource/v1/person/personId/personInfo");
        }

        public SubscribeByEventTypesResponse SubscribeByEventTypes(SubscribeByEventTypesRequest request)
        {
            return processAndSend<SubscribeByEventTypesRequest, SubscribeByEventTypesResponse>(request, "api/eventService/v1/eventSubscriptionByEventTypes");

        }

        public async Task<SubscribeByEventTypesResponse> SubscribeByEventTypesAsync(SubscribeByEventTypesRequest request)
        {
            return await processAndSendAsync<SubscribeByEventTypesRequest, SubscribeByEventTypesResponse>(request, "api/eventService/v1/eventSubscriptionByEventTypes");
        }

        public CancelSubscribeByEventTypesResponse CancelSubscriptionByEventTypes(CancelSubscribeByEventTypesRequest request)
        {
            return processAndSend<CancelSubscribeByEventTypesRequest, CancelSubscribeByEventTypesResponse>(request, "api/eventService/v1/eventUnSubscriptionByEventTypes");

        }

        public async Task<CancelSubscribeByEventTypesResponse> CancelSubscriptionByEventTypesAsync(CancelSubscribeByEventTypesRequest request)
        {
            return await processAndSendAsync<CancelSubscribeByEventTypesRequest, CancelSubscribeByEventTypesResponse>(request, "api/eventService/v1/eventUnSubscriptionByEventTypes");
        }

        public EventImageDataReponse GetEventImageData(EventImageDataRequest request)
        {
            return processAndSend<EventImageDataRequest, EventImageDataReponse>(request, "api/eventService/v1/image_data");

        }

        public async Task<EventImageDataReponse> GetEventImageDataAsync(EventImageDataRequest request)
        {
            return await processAndSendAsync<EventImageDataRequest, EventImageDataReponse>(request, "api/eventService/v1/image_data");
        }
        #endregion

        public event EventHandler<EventMessage> OnEventNotify;

        public virtual void receive()
        {
            SendEvent(new EventMessage()
            {
                Method = "POST"
            });
        }

        private void SendEvent(EventMessage eventMessage)
        {
            OnEventNotify?.Invoke(this, eventMessage);
        }

        private R processAndSend<T, R>(T request, string url)
        {
            string contentData = JsonSerializer.Serialize(request, options);
            HttpRequestMessage httpRequest = new HttpRequestMessage()
            {

                Method = HttpMethod.Post,
                RequestUri = new Uri(url, UriKind.Relative),
                Content = new StringContent(contentData, Encoding.UTF8, "application/json")
            };

            R response = AsyncHelper.RunSync<R>(() => sendAndExtractResponse<R>(httpRequest));
            return response;
        }

        private async Task<R> processAndSendAsync<T, R>(T request, string url)
        {
            string contentData = JsonSerializer.Serialize(request, options);
            HttpRequestMessage httpRequest = new HttpRequestMessage()
            {

                Method = HttpMethod.Post,
                RequestUri = new Uri(url, UriKind.Relative),
                Content = new StringContent(contentData, Encoding.UTF8, "application/json")
            };

            R response = await sendAndExtractResponse<R>(httpRequest);
            return response;
        }

        private async Task<T> sendAndExtractResponse<T>(HttpRequestMessage httpRequest)
        {
            try
            {
                HttpResponseMessage httpResponseMessage = await _httpClient.SendAsync(httpRequest);
                httpResponseMessage.EnsureSuccessStatusCode();
                var body = await httpResponseMessage.Content.ReadAsStreamAsync();
                T response = await JsonSerializer.DeserializeAsync<T>(body, options);
                return response;
            }
            catch (Exception e)
            {
                throw new ArtemisException(e.Message, e);
            }
        }

        public void StartServer(int tcpPort)
        {
            var server = new WebServer(tcpPort)
           .WithCors()
           .WithLocalSessionManager()
           .WithModule(new ActionModule("/", HttpVerbs.Any, ctx =>
           {
               byte[] requestBody = AsyncHelper.RunSync<byte[]>(() => ctx.GetRequestBodyAsByteArrayAsync());
               EventMessage eventMessage = JsonSerializer.Deserialize<EventMessage>(requestBody, options);
               SendEvent(eventMessage);
               return ctx.SendDataAsync(new { });
           }));
            // Listen for state changes.
            server.StateChanged += (s, e) => Console.WriteLine($"WebServer New State - {e.NewState}");
            Thread t = new Thread(new ThreadStart(() =>
            {
                Task task = server.RunAsync();
                task.Wait();
            }));
            t.Start();
        }
    }
}
