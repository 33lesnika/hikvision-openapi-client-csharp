﻿using System.ComponentModel.DataAnnotations;

namespace Hikvision_Artemis_Api
{
    public class Event
    {
        [Required]
        public string EventId { get; set; }
        [Required]
        public string SrcIndex { get; set; }
        [Required]
        public string SrcType { get; set; }
        public string SrcName { get; set; }
        [Required]
        public int Status { get; set; }
        public int EventLvl { get; set; }
        [Required]
        public int Timeout { get; set; }
        [Required]
        public string HappenTime { get; set; }
        public string SrcParentIdex { get; set; }
    }
}