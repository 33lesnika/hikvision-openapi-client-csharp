﻿using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;

namespace Hikvision_Artemis_Api
{
    public class ArtemisRequestSigner
    {
        private readonly string _apiKey;
        private readonly byte[] _apiSecret;

        public ArtemisRequestSigner(string apiKey, string apiSecret)
        {
            _apiKey = apiKey;
            _apiSecret = ToBytes(apiSecret);
        }

        private static readonly MediaTypeHeaderValue JSON_CONTENT_TYPE = MediaTypeHeaderValue.Parse("application/json; charset=UTF-8");

        public void Sign(HttpRequestMessage request)
        {
            const string acceptValue = "application/json";
            request.Headers.Remove("Accept");
            request.Headers.Add("Accept", acceptValue);
            request.Content.Headers.ContentType = JSON_CONTENT_TYPE;

            var nonceString = Guid.NewGuid().ToString();
            string currentTimeMs = CurrentTimestampMillis().ToString();

            string stringToSign = ComposeStringToSign(new SignData()
            {
                Method = request.Method.Method,
                Accept = ReadHeader(request.Headers, "Accept"),
                ContentType = request.Content.Headers.ContentType.ToString(),
                Nonce = nonceString,
                Timestamp = currentTimeMs,
                Path = request.RequestUri.PathAndQuery
            });
            string signature = ComputeSignature(stringToSign);

            AddSignatureHeaders(request, nonceString, currentTimeMs, signature);
        }

        private void AddSignatureHeaders(HttpRequestMessage request, string nonceString, string currentTimeMs, string signature)
        {
            request.Headers.Add("x-ca-key", _apiKey);
            request.Headers.Add("x-ca-nonce", nonceString);
            request.Headers.Add("x-ca-timestamp", currentTimeMs);
            request.Headers.Add("x-ca-signature-headers", "x-ca-key,x-ca-nonce,x-ca-timestamp");
            request.Headers.Add("x-ca-signature", signature);
        }

        public string ComputeSignature(string stringToSign)
        {
            byte[] bytesToSign = ToBytes(stringToSign);
            byte[] hashValue = Hash(bytesToSign);
            return Convert.ToBase64String(hashValue);
        }

        public string ComposeStringToSign(SignData signData)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendNewLine(signData.Method);
            sb.AppendNewLine(signData.Accept);
            sb.AppendNewLine(signData.ContentType);
            sb.Append("x-ca-key:").AppendNewLine(_apiKey);
            sb.Append("x-ca-nonce:").AppendNewLine(signData.Nonce);
            sb.Append("x-ca-timestamp:").AppendNewLine(signData.Timestamp);
            sb.Append(signData.Path);
            return sb.ToString();
        }

        private string ReadHeader(HttpRequestHeaders headers, string headerName)
        {
            return headers.TryGetValues(headerName, out var values) ? values.FirstOrDefault() : null;
        }

        public byte[] Hash(byte[] data)
        {
            using (HMACSHA256 hmac = new HMACSHA256(_apiSecret))
            {
                return hmac.ComputeHash(data);
            }
        }

        private static readonly DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public static long CurrentTimestampMillis()
        {
            return (long)(DateTime.UtcNow - epoch).TotalMilliseconds;
        }

        public static byte[] ToBytes(string str)
        {
            return Encoding.UTF8.GetBytes(str);
        }
    }

    public class SignData
    {
        public string Method { get; set; }
        public string Accept { get; set; }
        public string ContentType { get; set; }
        public string Nonce { get; set; }
        public string Timestamp { get; set; }
        public string Path { get; set; }
    }

    static class AppendNewLineStringBuilderExtension
    {
        public static StringBuilder AppendNewLine(this StringBuilder sb, string data)
        {
            return sb.Append(data).Append('\n');
        }
    }
}